const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Please provide a name'],
		trim: true
	},
	email: {
		type: String,
		required: [true, 'Please provide email'],
		unique: true,
		lowercase: true,
		validate: [validator.isEmail, 'Enter a valid email']
	},
	photo: {
		type: String
	},
	password: {
		type: String,
		required: [true, 'You cannot move forward without password'],
		minlength: 8
	},
	confirmPassword: {
		type: String,
		required: [true, 'Confirm your password please'],
		validate: {
			// this validator only works on .create() or .save() not on like findOneAndUpdate
			validator: function(el) {
				return el === this.password;
			},
			message: 'Password does not match'
		}
	}
});

userSchema.pre('save', async function(next) {
	if (!this.isModified('password')) return next();
	this.password = await bcrypt.hash(this.password, 12);
	//setting confirmPassword as undefined prevent it from saving into database
	this.confirmPassword = undefined;
});

const User = mongoose.model('User', userSchema);

module.exports = User;
