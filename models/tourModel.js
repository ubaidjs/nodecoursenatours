/* eslint-disable quotes */
/* eslint-disable quote-props */
const mongoose = require('mongoose');
const slugify = require('slugify');
// const validator = require('validator');

const tourSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, 'Tour name missing'],
			unique: true,
			trim: true
			//does not work properly, so left commented for refrence
			// validate: [validator.isAlpha, 'Name must only contain alpabets']
		},
		slug: String,
		duration: {
			type: Number,
			required: true
		},
		difficulty: {
			type: String,
			required: true
		},
		maxGroupSize: {
			type: Number,
			required: true
		},
		ratingsAverage: {
			type: Number,
			default: 0,
			min: [1, 'Rating must be above 1.0'],
			max: [5, 'Rating must be below 5.0']
		},
		ratingsQuantity: {
			type: Number,
			default: 0
		},
		price: {
			type: Number,
			required: [true, 'Tour price not provided']
		},
		priceDiscount: {
			type: Number,
			validate: {
				//this will work only for new document creation and not update
				validator: function(val) {
					return val < this.price;
				},
				message: 'discount should be smaller than price! idiot'
			}
		},
		summary: {
			type: String,
			trim: true
		},
		description: {
			type: String,
			trim: true
		},
		imageCover: {
			type: String,
			required: true
		},
		images: [String],
		createdAt: {
			type: Date,
			default: Date.now(),
			select: false // hides this property by default in result
		},
		startDates: [Date],
		hiddenTour: {
			type: Boolean,
			default: false
		}
	},
	{
		toJSON: { virtuals: true },
		toObject: { virtuals: true }
	}
);

//virtual property
tourSchema.virtual('durationInWeeks').get(function() {
	return this.duration / 7;
});

//Document Middleware - only runs before .save() and .create()
tourSchema.pre('save', function(next) {
	this.slug = slugify(this.name, { lower: true });
	next();
});

// tourSchema.post('save', function(doc, next) {
// 	console.log(doc);
// 	next();
// });

//QUERY MIDDLEWARE
tourSchema.pre(/^find/, function(next) {
	this.find({ hiddenTour: { $ne: true } });
	next();
});

//AGGREGATION MIDDLEWARE
tourSchema.pre('aggregate', function(next) {
	this.pipeline().unshift({ $match: { hiddenTour: { $ne: true } } });
	next();
});

//the first argument in mongoose.model is collection name which get
//converted to lowercase and pluralised.
const Tour = mongoose.model('tours', tourSchema);

module.exports = Tour;

// const testTour = new Tour({
// 	name: 'The Sea Explorer',
// 	rating: 4.8,
// 	price: 449
// });

// testTour
// 	.save()
// 	.then(doc => console.log(doc))
// 	.catch(() => console.log('Error saving data 🔥'));
