const express = require('express');
const tourController = require('./../controllers/tourController');

const router = express.Router();

// router.param('id', tourController.checkID);

//this is alias routing. it just pre-fill some query params
router
	.route('/top-5-cheap')
	.get(tourController.aliasTopTours, tourController.getAllTours);

router.route('/tour-stats').get(tourController.getTourStats);

router.route('/monthly-plan/:year').get(tourController.getMonthlyPlans);

router
	.route('/')
	.get(tourController.getAllTours)
	.post(tourController.addTour);

router
	.route('/:id')
	.get(tourController.getTour)
	.patch(tourController.editTour)
	.delete(tourController.deleteTour);

module.exports = router;
