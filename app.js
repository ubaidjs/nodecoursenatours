const express = require('express');

const AppError = require('./utils/appError');
const golbalErrorHandler = require('./controllers/errorController');
const tourRouter = require('./routes/tourRouter');
const userRouter = require('./routes/userRouter');

const app = express();

app.use(express.json());
app.use(express.static(`${__dirname}/public`));

app.use('/api/v1/tours/', tourRouter);
app.use('/api/v1/users/', userRouter);

// .all() is for all http methods eg. get post ...
// this code has to be put at the end otherwise no route will work
// catches all the unhandled routes and give error msg
app.all('*', (req, res, next) => {
	// res.status(404).json({
	// 	status: 'fail',
	// 	message: `Can't find ${req.originalUrl} on this server`
	// });

	// const err = new Error(`Can't find ${req.originalUrl} on this server`);
	// err.status = 'fail';
	// err.statusCode = 404;
	// next(err);

	next(new AppError(`Can't find ${req.originalUrl} on this server`, 404));
});

app.use(golbalErrorHandler);

module.exports = app;
