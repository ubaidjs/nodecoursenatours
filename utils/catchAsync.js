//exported as catchAsync, this function accept a function as parameter
//and return a function which calls the function recieved
module.exports = fn => {
	return (req, res, next) => {
		fn(req, res, next).catch(err => next(err));
	};
};
