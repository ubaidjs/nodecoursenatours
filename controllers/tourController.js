/* eslint-disable quotes */
/* eslint-disable quote-props */
const Tour = require('./../models/tourModel');
const ApiFeatures = require('./../utils/apiFeatures');
const catchAsync = require('./../utils/catchAsync');
const AppError = require('./../utils/appError');

exports.aliasTopTours = (req, res, next) => {
	req.query.limit = '5';
	req.query.sort = '-ratingsAverage,price';
	req.query.fields = 'name,price,ratingsAverage,summary,difficulty';
	next();
};

exports.getAllTours = catchAsync(async (req, res, next) => {
	//Execute query
	const features = new ApiFeatures(Tour.find(), req.query)
		.filter()
		.sort()
		.fields()
		.paginate();

	const tours = await features.query;
	res.status(200).json({
		status: 'success',
		results: tours.length,
		data: {
			tours
		}
	});
});

exports.getTour = catchAsync(async (req, res, next) => {
	const tour = await Tour.findById(req.params.id);

	if (!tour) {
		return next(new AppError('Oops! No tour found with that ID', 404));
	}

	res.status(200).json({
		status: 'success',
		data: {
			tour
		}
	});
});

exports.addTour = catchAsync(async (req, res, next) => {
	const newTour = await Tour.create(req.body);

	res.status(201).json({
		staus: 'success',
		data: {
			tour: newTour
		}
	});
	// try {
	// 	const newTour = await Tour.create(req.body);

	// 	res.status(201).json({
	// 		staus: 'success',
	// 		data: {
	// 			tour: newTour
	// 		}
	// 	});
	// } catch (error) {
	// 	res.status(400).json({
	// 		status: 'failed',
	// 		message: error
	// 	});
	// }
});

exports.editTour = catchAsync(async (req, res, next) => {
	const tour = await Tour.findByIdAndUpdate(req.params.id, req.body, {
		new: true,
		runValidators: true
	});

	if (!tour) {
		return next(new AppError('Oops! No tour found with that ID', 404));
	}

	res.status(200).json({
		status: 'success',
		data: {
			tour
		}
	});
});

exports.deleteTour = catchAsync(async (req, res, next) => {
	const tour = await Tour.findByIdAndDelete(req.params.id);

	if (!tour) {
		return next(new AppError('Oops! No tour found with that ID', 404));
	}

	res.status(200).json({
		status: 'success',
		data: 'Tour Deleted'
	});
});

exports.getTourStats = catchAsync(async (req, res, next) => {
	const stats = await Tour.aggregate([
		{
			$match: { ratingsAverage: { $gte: 4.5 } }
		},
		{
			$group: {
				// _id: null,
				_id: '$difficulty',
				numTours: { $sum: 1 },
				avgRating: { $avg: '$ratingsAverage' },
				avgPrice: { $avg: '$price' },
				minPrice: { $min: '$price' },
				maxPrice: { $max: '$price' }
			}
		},
		{
			$sort: { avgPrice: 1 }
		}
	]);

	res.status(200).json({
		status: 'success',
		data: {
			stats
		}
	});
});

exports.getMonthlyPlans = catchAsync(async (req, res, next) => {
	const year = req.params.year * 1;

	const plan = await Tour.aggregate([
		{
			$unwind: '$startDates'
		},
		{
			$match: {
				startDates: {
					$gte: new Date(`${year}-01-01`),
					$lte: new Date(`${year}-12-31`)
				}
			}
		},
		{
			$group: {
				_id: { $month: '$startDates' },
				totalTours: { $sum: 1 },
				tours: { $push: '$name' }
			}
		},
		{
			$addFields: { month: '$_id' }
		},
		{
			$project: { _id: 0 }
		},
		{
			$sort: { totalTours: -1 }
		}
	]);

	res.status(200).json({
		status: 'success',
		results: plan.length,
		data: {
			plan
		}
	});
});
