const AppError = require('./../utils/appError');

// for like invalid id input
const handleCastErrorDB = err => {
	const message = `Invalid ${err.path}: ${err.value}`;
	return new AppError(message, 400);
};

//for like duplicate tour name
const handleDuplicateFeildsDB = err => {
	const value = err.errmsg.match(/(["])(\\?.)*?\1/)[0];
	console.log(value);
	const message = `Duplicate feild value: ${value}. Please use another value`;
	return new AppError(message, 400);
};

//for invalid data input
const handleValidationErrorDB = err => {
	const errors = Object.values(err.errors).map(el => el.message);
	const message = `Invalid input data. ${errors.join('. ')}`;
	return new AppError(message, 400);
};

const sendErrorDev = (err, res) => {
	res.status(err.statusCode).json({
		status: err.status,
		error: err,
		message: err.message,
		stack: err.stack
	});
};

const sendErrorProd = (err, res) => {
	// operational errors
	if (err.isOperational) {
		res.status(err.statusCode).json({
			status: err.status,
			message: err.message
		});

		//programming or other unknown errors: dont leak error details
	} else {
		//log error
		console.error('ERROR', err);

		//send generic message
		res.status(500).json({
			status: 'error',
			message: 'Something went wrong'
		});
	}
};

module.exports = (err, req, res, next) => {
	err.statusCode = err.statusCode || 500;
	err.status = err.status || 'error';

	if (process.env.NODE_ENV === 'development') {
		sendErrorDev(err, res);
	} else if (process.env.NODE_ENV === 'production') {
		let error = { ...err };
		if (error.name === 'CastError') error = handleCastErrorDB(err);
		if (error.code === 11000) error = handleDuplicateFeildsDB(err);
		if (error.name === 'ValidationError') {
			error = handleValidationErrorDB(error);
		}
		sendErrorProd(error, res);
	}

	// res.status(err.statusCode).json({
	// 	status: err.status,
	// 	message: err.message
	// });
};
