const fs = require('fs');
const Tour = require('./../models/tourModel');

const tours = JSON.parse(
	fs.readFileSync(`${__dirname}/../dev-data/data/tours-simple.json`)
);

// All these functions all callback handler for the routes
exports.getAllTours = (req, res) => {
	res.status(200).json({
		status: 'success',
		results: tours.length,
		data: {
			tours: tours
		}
	});
};

exports.getTour = (req, res) => {
	const id = req.params.id * 1;
	const tour = tours.find(el => el.id === id);

	res.status(200).json({
		status: 'success',
		data: {
			tour
		}
	});
};

exports.addTour = (req, res) => {
	//the data is available at req.body
	const newId = tours[tours.length - 1].id + 1;
	const newTour = Object.assign({ id: newId }, req.body);

	tours.push(newTour);

	fs.writeFile(
		`${__dirname}/dev-data/data/tours-simple.json`,
		JSON.stringify(tours),
		err => {
			res.status(201).json({
				staus: 'success',
				data: {
					tour: newTour
				}
			});
		}
	);
};

exports.editTour = (req, res) => {
	//id to be used later
	const id = req.params.id * 1;

	res.status(200).json({
		status: 'success',
		data: 'updated tour here...'
	});
};

exports.deleteTour = (req, res) => {
	//id to be used later
	const id = req.params.id * 1;

	res.status(204).json({
		status: 'success',
		data: null
	});
};

exports.checkID = (req, res, next, val) => {
	console.log(`Tour id is: ${val}`);

	if (req.params.id * 1 > tours.length) {
		return res.status(404).json({
			status: 'fail',
			message: 'Invalid ID'
		});
	}
	next();
};
