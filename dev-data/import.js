// This is a standanlone file and does not participate in the application's
// working
const fs = require('fs');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const Tour = require('./../models/tourModel');

dotenv.config({ path: './config.env' });

const DB = process.env.DATABASE.replace(
	'<PASSWORD>',
	process.env.DATABASE_PASSWORD
);

mongoose
	.connect(DB, {
		useNewUrlParser: true,
		useCreateIndex: true,
		useFindAndModify: false
	})
	.then(connection => {
		console.log('Connected to database');
	})
	.catch(err => console.log('Error 🔴 connecting to database'));

//Read data file
const tours = JSON.parse(
	fs.readFileSync('dev-data/data/tours-simple.json', 'utf-8')
);

const importData = async () => {
	try {
		await Tour.create(tours);
		console.log('Data uploaded');
		process.exit();
	} catch (error) {
		console.log(error);
	}
};

const deleteData = async () => {
	try {
		await Tour.deleteMany();
		console.log('Data deleted');
		process.exit();
	} catch (error) {
		console.log(error);
	}
};

if (process.argv[2] === '--import') {
	importData();
} else if (process.argv[2] === '--delete') {
	deleteData();
}
