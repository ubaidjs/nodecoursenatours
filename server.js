const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config({ path: './config.env' });

process.on('uncaughtException', err => {
	console.log('UNCAUGHT EXCEPTION 🔥 SHUTTING DOWN....');
	console.log(err.name, err.message);
	process.exit(1);
});

const app = require('./app');

const DB = process.env.DATABASE.replace(
	'<PASSWORD>',
	process.env.DATABASE_PASSWORD
);

// start is defined just to calculate database connection time
const start = Date.now();

mongoose
	.connect(DB, {
		useNewUrlParser: true,
		useCreateIndex: true,
		useFindAndModify: false
	})
	.then(connection => {
		console.log((Date.now() - start) / 1000, 'Connected to database');
	});
// .catch(err => console.log('Error connecting to database'));

//application is running on port 3000
const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
	console.log(`App running on port ${port}`);
});

process.on('unhandledRejection', err => {
	console.log('UNHANDLED REJECTION 🔥 SHUTTING DOWN....');
	console.log(err.name, err.message);
	server.close(() => {
		process.exit(1);
	});
});
